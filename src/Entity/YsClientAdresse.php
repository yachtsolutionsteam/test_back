<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\AuthorRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;


/**
 * YsClientAdresse
 * @ORM\Table(name="ys_client_adresse", indexes={@ORM\Index(name="uid", columns={"ys_client_id"})})
 * @ORM\Entity(repositoryClass=App\Repository\YsClientAdresseRepository::class)
 * @ApiResource()
 */
class YsClientAdresse
{
    /**
     * @var int
     * @Groups({"YsClient:read"})
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var bool
     * @Groups({"YsClient:read"})
     * @ORM\Column(name="livraison", type="boolean", nullable=false, options={"default"="b'0'"})
     */
    private $livraison = 'b\'0\'';

    /**
     * @var bool
     * @Groups({"YsClient:read"})
     * @ORM\Column(name="facturation", type="boolean", nullable=false, options={"default"="b'0'"})
     */
    private $facturation = 'b\'0\'';

    /**
     * @var string
     * @Groups({"YsClient:read"})
     * @ORM\Column(name="titre", type="string", length=255, nullable=false)
     */
    private $titre;

    /**
     * @var string
     * @Groups({"YsClient:read"})
     * @ORM\Column(name="nom", type="string", length=255, nullable=false)
     */
    private $nom;

    /**
     * @var string
     * @Groups({"YsClient:read"})
     * @ORM\Column(name="adresse", type="text", length=65535, nullable=false)
     */
    private $adresse;

    /**
     * @var string
     * @Groups({"YsClient:read"})
     * @ORM\Column(name="bp", type="string", length=100, nullable=false)
     */
    private $bp;

    /**
     * @var string
     * @Groups({"YsClient:read"})
     * @ORM\Column(name="cp", type="string", length=12, nullable=false)
     */
    private $cp;

    /**
     * @var string
     * @Groups({"YsClient:read"})
     * @ORM\Column(name="ville", type="string", length=120, nullable=false)
     */
    private $ville;

    /**
     * @var string|null
     * @Groups({"YsClient:read"})
     * @ORM\Column(name="pays", type="string", length=3, nullable=true, options={"fixed"=true})
     */
    private $pays;

    /**
     * @var string|null
     * @Groups({"YsClient:read"})
     * @ORM\Column(name="type", type="string", length=0, nullable=true)
     */
    private $type;

    /**
     * @var string
     * @Groups({"YsClient:read"})
     * @ORM\Column(name="tva_intracomm", type="string", length=255, nullable=false)
     */
    private $tvaIntracomm;

    /**
     * @var bool
     * @Groups({"YsClient:read"})
     * @ORM\Column(name="active", type="boolean", nullable=false, options={"default"="b'1'"})
     */
    private $active = 'b\'1\'';

    /**
     * @var string
     * @Groups({"YsClient:read"})
     * @ORM\Column(name="compte_client", type="string", length=255, nullable=false)
     */
    private $compteClient = '';

    /**
     * @var \YsClient
     * @ORM\ManyToOne(targetEntity="YsClient", inversedBy="ysClientAdresses")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ys_client_id", referencedColumnName="id")
     * })
     */
    private $ysClient;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLivraison(): ?bool
    {
        return $this->livraison;
    }

    public function setLivraison(bool $livraison): self
    {
        $this->livraison = $livraison;

        return $this;
    }

    public function getFacturation(): ?bool
    {
        return $this->facturation;
    }

    public function setFacturation(bool $facturation): self
    {
        $this->facturation = $facturation;

        return $this;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getBp(): ?string
    {
        return $this->bp;
    }

    public function setBp(string $bp): self
    {
        $this->bp = $bp;

        return $this;
    }

    public function getCp(): ?string
    {
        return $this->cp;
    }

    public function setCp(string $cp): self
    {
        $this->cp = $cp;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getPays(): ?string
    {
        return $this->pays;
    }

    public function setPays(?string $pays): self
    {
        $this->pays = $pays;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getTvaIntracomm(): ?string
    {
        return $this->tvaIntracomm;
    }

    public function setTvaIntracomm(string $tvaIntracomm): self
    {
        $this->tvaIntracomm = $tvaIntracomm;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getCompteClient(): ?string
    {
        return $this->compteClient;
    }

    public function setCompteClient(string $compteClient): self
    {
        $this->compteClient = $compteClient;

        return $this;
    }

    public function getYsClient(): ?YsClient
    {
        return $this->ysClient;
    }

    public function setYsClient(?YsClient $ysClient): self
    {
        $this->ysClient = $ysClient;

        return $this;
    }


}
