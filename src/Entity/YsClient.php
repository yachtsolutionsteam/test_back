<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\AuthorRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;


/**
 * YsClient
 * @ORM\Table(name="ys_client", indexes={@ORM\Index(name="agent_id", columns={"agent_id"})})
 * @ORM\Entity(repositoryClass=App\Repository\YsClientRepository::class)
 * @ApiResource(normalizationContext={"groups"={"YsClient:read"}})
 */
class YsClient
{
    /**
     * @var int
     * @Groups({"YsClient:read"})
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var bool
     * @Groups({"YsClient:read"})
     * @ORM\Column(name="genre", type="boolean", nullable=false)
     */
    private $genre;

    /**
     * @var string
     * @Groups({"YsClient:read"})
     * @ORM\Column(name="prenom", type="string", length=200, nullable=false)
     */
    private $prenom;

    /**
     * @var string
     * @Groups({"YsClient:read"})
     * @ORM\Column(name="nom", type="string", length=200, nullable=false)
     */
    private $nom;

    /**
     * @var string
     * @Groups({"YsClient:read"})
     * @ORM\Column(name="langue", type="string", length=2, nullable=false, options={"fixed"=true})
     */
    private $langue;

    /**
     * @var string
     * @Groups({"YsClient:read"})
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     */
    private $email;

    /**
     * @var string
     * @Groups({"YsClient:read"})
     * @ORM\Column(name="motdepasse", type="string", length=128, nullable=false)
     */
    private $motdepasse = '';

    /**
     * @var string
     * @Groups({"YsClient:read"})
     * @ORM\Column(name="titre_bienvenue", type="string", length=255, nullable=false)
     */
    private $titreBienvenue = '';

    /**
     * @var string
     * @Groups({"YsClient:read"})
     * @ORM\Column(name="texte_bienvenue", type="text", length=65535, nullable=false)
     */
    private $texteBienvenue;

    /**
     * @var string
     * @Groups({"YsClient:read"})
     * @ORM\Column(name="note", type="text", length=255, nullable=false)
     */
    private $note;

    /**
     * @var bool
     * @Groups({"YsClient:read"})
     * @ORM\Column(name="is_agent", type="boolean", nullable=false, options={"default"="b'0'"})
     */
    private $isAgent = 'b\'0\'';

    /**
     * @var \DateTime|null
     * @Groups({"YsClient:read"})
     * @ORM\Column(name="date_derniere_connexion", type="datetime", nullable=true)
     */
    private $dateDerniereConnexion;

    /**
     * @var bool
     * @Groups({"YsClient:read"})
     * @ORM\Column(name="active", type="boolean", nullable=false, options={"default"="b'0'"})
     */
    private $active = 'b\'0\'';

    /**
     * @var bool
     * @Groups({"YsClient:read"})
     * @ORM\Column(name="deleted", type="boolean", nullable=false, options={"default"="b'0'"})
     */
    private $deleted = 'b\'0\'';

    /**
     * @var bool
     * @Groups({"YsClient:read"})
     * @ORM\Column(name="new", type="boolean", nullable=false, options={"default"="b'0'"})
     */
    private $new = 'b\'0\'';

    /**
     * @var string
     * @Groups({"YsClient:read"})
     * @ORM\Column(name="brand", type="string", length=0, nullable=false)
     */
    private $brand;

    /**
     * @var string
     * @Groups({"YsClient:read"})
     * @ORM\Column(name="compte_client", type="string", length=255, nullable=false)
     */
    private $compteClient = '';

    /**
     * @var string
     * @Groups({"YsClient:read"})
     * @ORM\Column(name="concessionnaire", type="string", length=255, nullable=false)
     */
    private $concessionnaire = '';

    /**
     * @var string
     * @Groups({"YsClient:read"})
     * @ORM\Column(name="concessionnaire_pays", type="string", length=2, nullable=false, options={"fixed"=true})
     */
    private $concessionnairePays = '';

    /**
     * @var int|null
     * @Groups({"YsClient:read"})
     * @ORM\Column(name="brand_id", type="integer", nullable=true, options={"unsigned"=true})
     */
    private $brandId;

    /**
     * @var \YsClient
     * @Groups({"YsClient:read"})
     * @ORM\ManyToOne(targetEntity="YsClient")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="agent_id", referencedColumnName="id")
     * })
     */
    private $agent;

    /**
     * @ORM\OneToMany(targetEntity=YsClientAdresse::class, mappedBy="ysClient")
     * @Groups({"YsClient:read"})
     */
    private $ysClientAdresses;

    public function __construct()
    {
        $this->ysClientAdresses = new ArrayCollection();

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGenre(): ?bool
    {
        return $this->genre;
    }

    public function setGenre(bool $genre): self
    {
        $this->genre = $genre;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getLangue(): ?string
    {
        return $this->langue;
    }

    public function setLangue(string $langue): self
    {
        $this->langue = $langue;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getMotdepasse(): ?string
    {
        return $this->motdepasse;
    }

    public function setMotdepasse(string $motdepasse): self
    {
        $this->motdepasse = $motdepasse;

        return $this;
    }

    public function getTitreBienvenue(): ?string
    {
        return $this->titreBienvenue;
    }

    public function setTitreBienvenue(string $titreBienvenue): self
    {
        $this->titreBienvenue = $titreBienvenue;

        return $this;
    }

    public function getTexteBienvenue(): ?string
    {
        return $this->texteBienvenue;
    }

    public function setTexteBienvenue(string $texteBienvenue): self
    {
        $this->texteBienvenue = $texteBienvenue;

        return $this;
    }

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function setNote(string $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getIsAgent(): ?bool
    {
        return $this->isAgent;
    }

    public function setIsAgent(bool $isAgent): self
    {
        $this->isAgent = $isAgent;

        return $this;
    }

    public function getDateDerniereConnexion(): ?\DateTimeInterface
    {
        return $this->dateDerniereConnexion;
    }

    public function setDateDerniereConnexion(?\DateTimeInterface $dateDerniereConnexion): self
    {
        $this->dateDerniereConnexion = $dateDerniereConnexion;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getDeleted(): ?bool
    {
        return $this->deleted;
    }

    public function setDeleted(bool $deleted): self
    {
        $this->deleted = $deleted;

        return $this;
    }

    public function getNew(): ?bool
    {
        return $this->new;
    }

    public function setNew(bool $new): self
    {
        $this->new = $new;

        return $this;
    }

    public function getBrand(): ?string
    {
        return $this->brand;
    }

    public function setBrand(string $brand): self
    {
        $this->brand = $brand;

        return $this;
    }

    public function getCompteClient(): ?string
    {
        return $this->compteClient;
    }

    public function setCompteClient(string $compteClient): self
    {
        $this->compteClient = $compteClient;

        return $this;
    }

    public function getConcessionnaire(): ?string
    {
        return $this->concessionnaire;
    }

    public function setConcessionnaire(string $concessionnaire): self
    {
        $this->concessionnaire = $concessionnaire;

        return $this;
    }

    public function getConcessionnairePays(): ?string
    {
        return $this->concessionnairePays;
    }

    public function setConcessionnairePays(string $concessionnairePays): self
    {
        $this->concessionnairePays = $concessionnairePays;

        return $this;
    }

    public function getBrandId(): ?int
    {
        return $this->brandId;
    }

    public function setBrandId(?int $brandId): self
    {
        $this->brandId = $brandId;

        return $this;
    }

    public function getAgent(): ?self
    {
        return $this->agent;
    }

    public function setAgent(?self $agent): self
    {
        $this->agent = $agent;

        return $this;
    }

    /**
     * @return Collection|YsClientAdresse[]
     */
    public function getYsClientAdresses(): Collection
    {
        return $this->ysClientAdresses;
    }

//    public function addBook(YsClientAdresse $ysClientAdresse): self
//    {
//        if (!$this->ysClientAdresses->contains($ysClientAdresse)) {
//            $this->ysClientAdresses[] = $ysClientAdresse;
//            $ysClientAdresse->setAuthor($this);
//        }
//
//        return $this;
//    }
//
//    public function removeBook(Book $book): self
//    {
//        if ($this->books->removeElement($book)) {
//            // set the owning side to null (unless already changed)
//            if ($book->getAuthor() === $this) {
//                $book->setAuthor(null);
//            }
//        }
//
//        return $this;
//    }

}
