<?php

namespace App\Repository;

use App\Entity\YsClientAdresse;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method YsClientAdresse|null find($id, $lockMode = null, $lockVersion = null)
 * @method YsClientAdresse|null findOneBy(array $criteria, array $orderBy = null)
 * @method YsClientAdresse[]    findAll()
 * @method YsClientAdresse[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class YsClientAdresseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, YsClientAdresse::class);
    }

    // /**
    //  * @return YsClientAdresse[] Returns an array of YsClientAdresse objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('y')
            ->andWhere('y.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('y.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?YsClientAdresse
    {
        return $this->createQueryBuilder('y')
            ->andWhere('y.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
