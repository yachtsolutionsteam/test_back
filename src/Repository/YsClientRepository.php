<?php

namespace App\Repository;

use App\Entity\YsClient;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method YsClient|null find($id, $lockMode = null, $lockVersion = null)
 * @method YsClient|null findOneBy(array $criteria, array $orderBy = null)
 * @method YsClient[]    findAll()
 * @method YsClient[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class YsClientRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, YsClient::class);
    }

    // /**
    //  * @return YsClient[] Returns an array of YsClient objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('y')
            ->andWhere('y.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('y.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?YsClient
    {
        return $this->createQueryBuilder('y')
            ->andWhere('y.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
